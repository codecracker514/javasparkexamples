package com.example;


import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

import java.util.Map;


/*
* Learning Objectives:
* 1. How to create a PairRDD
* 2. Use byKey functions of pairRDD
* 3. Save data of Pair RDD
* 4. Use collectAsMap on PairRDD to get all values of RDD to the driver
*
* */


final public class StockAnalyzer {


    public static void main(String[] args) throws Exception {

        if (args.length < 2) {
            System.err.println("Usage: com.example.StockAnalyzer <input> <output>");
            System.exit(-1);
        }
        SparkConf sparkConf = new SparkConf().setAppName("Stock Analyzer");
        sparkConf.setIfMissing("spark.master", "local[*]");
        sparkConf.set("spark.hadoop.validateOutputSpecs", "false");
        JavaSparkContext sc = new JavaSparkContext(sparkConf);


        JavaRDD<String> stocksRdd = sc.textFile(args[0], 1);

        for (String r : stocksRdd.take(10)) {
            System.out.println(r);
        }

        JavaRDD<String> stocksData = stocksRdd.filter(new Function<String, Boolean>() {
            public Boolean call(String s) throws Exception {
                return !s.startsWith("date");
            }
        });

        System.out.println("Stocks data after filtering out header");

        for (String r : stocksData.take(10)) {
            System.out.println(r);
        }

        JavaPairRDD<String, Double> symbolVolumeRdd = stocksData.mapToPair(new PairFunction<String, String, Double>() {
            public Tuple2<String, Double> call(String s) {
                String[] tokens = s.split(",");
                return new Tuple2<String, Double>(tokens[7], Double.parseDouble(tokens[5]));
            }
        });


        JavaPairRDD<String, Iterable<Double>> symbolGrouped = symbolVolumeRdd.groupByKey();
        /*
        JavaPairRDD<String, Double> symbolAvgVolume = symbolGrouped.mapValues(new Function<Iterable<Double>, Double>() {

            public Double call(Iterable<Double> values) {
                Double sum = 0.0;
                int count = 0;
                for (Double r : values) {
                    sum += r;
                    ++count;
                }
                return sum / count;
            }

        });*/

        JavaPairRDD<String, Double> symbolAvgVolume = symbolGrouped.mapValues((values) -> {
            Double sum = 0.0;
            int count = 0;
            for (Double r : values) {
                sum += r;
                ++count;
            }
            return sum / count;
        });

        System.out.println("\n\nSaving the stock average volume to HDFS ...");
        symbolAvgVolume.saveAsTextFile(args[1]);


        System.out.println("\n\nAverage volume per stock symbol...");

//        for(Tuple2<String, Double> r: symbolAvgVolume.collect()){
//            System.out.println(r._1() + ": " + r._2().toString());
//        }

        Map<String, Double> symbolAvgVolumeMap = symbolAvgVolume.collectAsMap();
        for (String key : symbolAvgVolumeMap.keySet()) {
            System.out.println("Symbol: " + key + " value: " + symbolAvgVolumeMap.get(key));
        }

        System.out.print("Process is complete. Press any key to exit.");

        System.in.read();
        sc.stop();
        sc.close();

    }


}
